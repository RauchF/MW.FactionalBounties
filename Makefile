all: build

outdir:
	mkdir -p out

esp: outdir
	tes3conv "Factional Bounties.json" "out/Factional Bounties.esp"

build: clean outdir esp
	cp -r MWSE out/MWSE

zip: all
	cd out && \
	zip -r ../out.zip "Factional Bounties.esp" MWSE/mods/robocroque/FactionalBounties && \
	cd ..

copy: all
	./copy_to_mo_folder.sh

clean:
	rm -rf out
	rm -f out.zip

#!/bin/env bash

loadEnv() {
  local envFile="${1?Missing environment file}"
  local environmentAsArray variableDeclaration
  mapfile environmentAsArray < <(
    grep --invert-match '^#' "${envFile}" \
      | grep --invert-match '^\s*$'
  ) # Uses grep to remove commented and blank lines
  for variableDeclaration in "${environmentAsArray[@]}"; do
    export "${variableDeclaration//[$'\r\n']}" # The substitution removes the line breaks
  done
}

loadEnv .env

if [ "$MO_MOD_PATH" = "" ]
then
    >&2 echo "MO_MOD_PATH environment variable not set! Did your .env not get sourced?"
    exit 1
fi

rsync -vur --delete --exclude=/meta.ini ./out/ "$MO_MOD_PATH"